//#define XERR
#include "main.ih"

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        cout << "need filename\n";
        return 1;
    }

    Scanner scanner{ argv[1], "" };

    while (scanner.lex())
        ;
}
